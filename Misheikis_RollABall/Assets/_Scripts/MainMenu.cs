﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame ()
    {
        //This allows unity to load the RollABAll scene from the title screen
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    //This promts Unity to quit if the player presses the quit button on the main menu
    public void QuitGame ()
    {
        Debug.Log("Quit");
        //This debug is just for me to see if the application will actually quit if player clicks on quit button
        Application.Quit();
    
     

    }
}

